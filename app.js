const express = require('express');
const app = express();
const path = require('path');
//const { v4: uuid } = require('uuid');

app.use(express.static('public'));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

let homeFlag = false;

app.get('/', (req, res) => {
    console.log("homeeeee");
    res.render('home.ejs', { flag: homeFlag })
    //res.send("hi")
})

var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.post('/', urlencodedParser, (req, res) => {

    // res.send("<h1>hi</h1>")
    const { username, password } = req.body;
    res.redirect('/welcome?username=' + username)

})

app.get('/welcome', (req, res) => {
    console.log("welcome");
    let { username } = req.query;
    res.render('welcome.ejs', { username })
    console.log(username)

})



const port = process.env.PORT || 3000
app.listen(port, () => {
    console.log("Listening!");
})